package br.com.chronosacademy.media;

public class Media {


    public String resultadoMedia(double nota1, double nota2) {
        double media = (nota1 + nota2) /2;

        if (media < 5) {
            return "Reprovado";
        }
        return "Aprovado";
    }
}
