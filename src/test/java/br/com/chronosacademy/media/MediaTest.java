package br.com.chronosacademy.media;

import org.junit.Test;

import static org.junit.Assert.*;

public class MediaTest {
    @Test
    public void validaAprovado() {
        double nota1 = 5;
        double nota2 = 5;

        Media media = new Media();
        String resultado = media.resultadoMedia(nota1, nota2);

    assertEquals("Aprovado", resultado);
    }


    @Test
    public void validaReprovado(){
        double nota1 = 4.9;
        double nota2 = 4.9;

        Media media = new Media();
        String resultado = media.resultadoMedia(nota1, nota2);

        assertEquals("Reprovado", resultado);

    }

}